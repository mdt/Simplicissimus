## GUI

| name | type | source | documentation | license | base | chat |
|------|------|--------|---------------|---------|------|------|
| dear pygui |  | https://github.com/hoffstadt/DearPyGui | https://dearpygui.readthedocs.io/en/latest/index.html | MIT | OpenGL | discord |
| fltk | c api | https://www.fltk.org/ | https://www.fltk.org/documentation.php | GPL | X11 | matrix |
| flutter |  | https://docs.flutter.dev/ | ? | BSD | multi | twitter |
| fyne |  | https://fyne.io/ | https://developer.fyne.io/ | BSD |  | slack, discord |
| godot |  | https://godotengine.org/ | https://docs.godotengine.org/en/stable/ | MIT | multi | rocket, irc |
| guizero |  |  |  | tkinter |  |
| gtk+ |  | https://gitlab.gnome.org/GNOME/gtk/ | https://www.gtk.org/docs/ | GPL | X11, Wayland | discourse |
| haxe |  | http://haxe.org/ | https://haxe.org/manual/introduction.html | MIT, GPL | multi | discord, gitter |
| kivy |  | https://kivy.org/#home | https://kivy.org/doc/stable/ | MIT | multi | discord |
| lara |  | https://github.com/integrativesoft/lara |  | Apache | js |
| löve |  |  |  |  |  |
| md python designer |  | https://labdeck.com/python-designer/ |  | Non free |  |
| prima |  | http://www.prima.eu.org/ |
| pyglet |  | https://bitbucket.org/pyglet/pyglet/wiki/Home |  | BSD |
| pyqt |  | https://riverbankcomputing.com/software/pyqt/intro |  | Non free |  |
| pyside |  | http://wiki.qt.io/Qt_for_Python |
| pysimplegui |  | https://github.com/MikeTheWatchGuy/PySimpleGUI |  |
| quasar |  | https://quasar-framework.org/ |  |  | vue.js |
| remi | browser | https://github.com/rawpython/remi |  | Apache | browser |  |
| sciter |  | https://sciter.com/ |
| swing |  | http://www.oracle.com/technetwork/java/javase/downloads/index.html |
| tkinter | tcl |  |  | BSD | X11 |  |
| wxpython / wxwidgets |  |  | https://wxwidgets.org/ |
| xojo |  | https://www.xojo.com/ |

